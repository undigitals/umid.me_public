import gql from "graphql-tag";

export const CardsQuery = gql`
  {
    posts {
      id
      title
      subtitle
      image {
        handle
      }
      subtitle
      caption
      logo {
        handle
      }
      content
    }
  }
`;
