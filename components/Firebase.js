import * as firebase from "firebase";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyAW5YRhZ9B8iRI2glpCN0vD-9014brB-YY",
  authDomain: "flexiuz-461a0.firebaseapp.com",
  databaseURL: "https://flexiuz-461a0.firebaseio.com",
  storageBucket: "flexiuz-461a0.appspot.com"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
