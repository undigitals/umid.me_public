import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import HomeScreen from "./screens/HomeScreen";
import AppNavigator from "./navigator/AppNavigator";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

const initialState = {
  action: "",
  name: ""
};

const client = new ApolloClient({
  uri: `https://api-apeast.graphcms.com/v1/cjst65w7a4yyb01fjo4drrecw/master`,
  // uri: `localhost:4444`,
  credentials: "same-origin"
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "OPEN_MENU":
      return { action: "openMenu" };
    case "CLOSE_MENU":
      return { action: "closeMenu" };
    case "UPDATE_NAME":
      return { name: action.name };
    case "OPEN_CARD":
      return { action: "openCard" };
    case "CLOSE_CARD":
      return { action: "closeCard" };
    case "OPEN_LOGIN":
      return { action: "openLogin" };
    case "CLOSE_LOGIN":
      return { action: "closeLogin" };
    default:
      return state;
  }
};

const store = createStore(reducer);

const App = () => (
  <ApolloProvider client={client}>
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  </ApolloProvider>
);

export default App;
